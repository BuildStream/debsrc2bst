#  Copyright (C) 2017 Codethink Limited
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU Lesser General Public
#  License as published by the Free Software Foundation; either
#  version 2 of the License, or (at your option) any later version.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library. If not, see <http://www.gnu.org/licenses/>.
#
#  Authors:
#        Jonathan Maw <jonathan.maw@codethink.co.uk>

import os
import yaml
import re
from collections import OrderedDict
from . import parse_controldata, CONTROL_FILE


def get_build_depends(sourcedata):
    bdtext = sourcedata["source"].get("Build-Depends", "")
    bditext = sourcedata["source"].get("Build-Depends-Indep", "")
    builddeps = [
        x.strip().split(" ")[0] for x in bdtext.split(",")
    ] + [
        x.strip().split(" ")[0] for x in bditext.split(",")
    ]
    return builddeps


def get_depends(sourcedata):
    depends = set()
    for package in sourcedata["packages"].values():
        deps = package.get("Depends", "").split(",")
        deps += package.get("Pre-Depends", "").split(",")

        for dep in deps:
            if re.match("^\$\{.+\}$", dep):
                print("Package {} has the macro '{}' which is impossible to expand"
                      .format(package["Package"], dep))
            elif "|" in dep:
                newdep = dep.strip().split(" ")[0]
                print("Package {} has the either-or dependency '{}', using '{}'"
                      .format(package["Package"], dep, newdep))
                depends.add(newdep)
            else:
                newdep = dep.strip().split(" ")[0]
                depends.add(newdep)

    return depends

def get_external_depends(sourcedata):
    # We don't separate out packages in buildstream, so any dependencies
    # happening within a source are redundant
    provided_packages = ([p["Package"] for p in sourcedata["packages"].values()]
                         + [p["Provides"] for p in sourcedata["packages"].values() if "Provides" in p])
    return [d for d in get_depends(sourcedata) if d not in provided_packages]


def quoted(words):
    return ["'{}'".format(x) for x in words]


def calculate_depends(providedata, sourcedata):
    depends = {}
    unmet_build_depends = set()
    unmet_depends = set()
    for bd in get_build_depends(sourcedata):
        if bd in providedata:
            if providedata[bd] not in depends:
                depends[providedata[bd]] = {
                    "filename": providedata[bd],
                    "type": "build"
                }
        elif bd:
            unmet_build_depends.add(bd)

    for d in get_external_depends(sourcedata):
        if d in providedata:
            if providedata[d] not in depends:
                depends[providedata[d]] = {
                    "filename": providedata[d],
                    "type": "runtime"
                }
            elif (type(depends[providedata[d]]) == dict and depends[providedata[d]]["type"] == "build"):
                # buildstream doesn't read depends of type 'all'
                # instead, they are defined without type.
                depends[providedata[d]] = providedata[d]
        elif d:
            unmet_depends.add(d)

    if unmet_build_depends:
        print("{} could not meet these build-depends: {}"
              .format(sourcedata["bstfile"], " ".join(sorted(quoted(unmet_build_depends)))))
    if unmet_depends:
        print("{} could not meet these depends: {}"
              .format(sourcedata["bstfile"], " ".join(sorted(quoted(unmet_depends)))))

    return [x for x in depends.values()]


def calculate_bst_contents(args, sourcedata, providedata):
    bst_content = OrderedDict()
    bst_content['kind'] = "dpkg_build"
    bst_content['description'] = ("dpkg build based on {}"
                                  .format(sourcedata["source"]["Source"]))
    if args.get("base-build-depend", False):
        bst_content['depends'] = [{"filename": args["debian-base"],
                                 "type": "build"}]
    else:
        bst_content['depends'] = [args["debian-base"]]
    bst_content['depends'] += calculate_depends(providedata, sourcedata)

    if os.path.exists(os.path.join(sourcedata['repository'], ".git")):
        from . import git_source_data
        bst_content['sources'] = [git_source_data(sourcedata['repository'])]
    else:
        bst_content['sources'] = []
        print("Repository {}'s version control system is unsupported. Please add it manually.".format(sourcedata['repository']))

    return bst_content


def write_bst(args, sourcedata, providedata):
    contents = calculate_bst_contents(args, sourcedata, providedata)
    yaml.add_representer(OrderedDict, lambda self, data: self.represent_mapping('tag:yaml.org,2002:map', data.items()))
    dstpath = os.path.join(args['output-dir'], sourcedata["bstfile"])
    os.makedirs(os.path.dirname(dstpath), exist_ok=True)
    with open(dstpath, "w") as f:
        f.write(yaml.dump(contents, default_flow_style=False))


def validate(args):
    assert(os.path.isdir(args["output-dir"]))
    assert(os.access(args["output-dir"], os.W_OK))
    for repo in args["repositories"]:
        controlpath = os.path.join(repo, CONTROL_FILE)
        assert(os.path.isdir(repo))
        assert(os.path.isfile(controlpath))
        assert(os.access(controlpath, os.R_OK))


def generate_providedby(sourcesdata):
    providedby = {}
    for source in sourcesdata.values():
        for package in source["packages"].values():
            providedby[package["Package"]] = source["bstfile"]
            if "Provides" in package:
                providedby[package["Provides"]] = source["bstfile"]
    return providedby


def generate(args):
    sources_data = {}
    for d in args["repositories"]:
        data = parse_controldata(args, d)
        sources_data[data["source"]["Source"]] = data

    providedby = generate_providedby(sources_data)
    for source in sources_data.values():
        write_bst(args, source, providedby)
