#!/usr/bin/python3
#
#  Copyright (C) 2017 Codethink Limited
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU Lesser General Public
#  License as published by the Free Software Foundation; either
#  version 2 of the License, or (at your option) any later version.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library. If not, see <http://www.gnu.org/licenses/>.
#
#  Authors:
#        Jonathan Maw <jonathan.maw@codethink.co.uk>

import argparse
from debsrc2bst import generate, validate

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Generate bst files from a set of repositories")
    parser.add_argument("-b", "--debian-base", required=True,
                        help="Element path that all dpkg-build elements use as 'base'")
    parser.add_argument("--base-build-depend", action="store_true",
                        help="Whether the 'base' element is of type 'build'. If not, it will be of type 'all'")
    parser.add_argument("-o", "--output-dir", required=True,
                        help="Directory to generate output bst files in")
    parser.add_argument("-p", "--element-prefix", help="Prefix to prepend to all generated element paths")
    parser.add_argument("repository", nargs="+", help="Directory containing a debian source")
    args = parser.parse_args()
    argdata = {
        "debian-base": args.debian_base,
        "output-dir": args.output_dir,
        "repositories": args.repository,
        "element-prefix": args.element_prefix or None,
        "base-build-depend": args.base_build_depend or None
    }
    validate(argdata)
    generate(argdata)
