Debsrc2bst
==========

This is a tool to generate a .bst for a given list of directories
that contain debian sources.
(a debian source being defined as a directory with a "debian"
subdirectory containing all the usual metadata of a debian source).

It will attempt to resolve dependency information between the packages
provided by reading their debian/control data.
